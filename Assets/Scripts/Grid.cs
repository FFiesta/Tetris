﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {

    static AudioClip auRowExplode;
    public static int w = 10;
    public static int h = 26;

    public static Transform[,] grid = new Transform[w, h];
    
    public static Vector2 RoundVec2(Vector2 v)
    {
        return new Vector2(Mathf.Round(v.x), Mathf.Round(v.y));
    }

    public static bool InsideBorder(Vector2 pos)
    {
        return ((int)pos.x >= 0 
            && (int)pos.x < w 
            && (int)pos.y >= 0);
    }

    public static void DeleteRow(int y)
    {
        for(int x = 0; x < w; x++)
        {
            SpawnExplosion(grid[x, y]);     // Spawn explosion.

            Destroy(grid[x, y].gameObject); // Dissapear cube.

            grid[x, y] = null;              // Clean it from the grid.
        }

    }
    

    public static void DecreaseRow(int y)
    {
        for (int x = 0; x < w; x++)
        {
            if (grid[x, y] != null)
            {
                grid[x, y - 1] = grid[x, y];
                grid[x, y] = null;

                grid[x, y - 1].position += new Vector3(0, -1, 0);
            }
        }
    }

    public static void DecreaseRowsAbove(int y)
    {
        for (int i = y; i < h; i++)
            DecreaseRow(i);
        
    }

    public static bool IsRowFull(int y)
    {
        for (int x = 0; x < w; x++)
            if (grid[x, y] == null)
                return false;
        
        return true;
    }

    public static void DeleteFullRows()
    {
        int giveScore = 0;
        for (int y = 0; y < h; y++)
        {
            if (IsRowFull(y))
            {
                DeleteRow(y);
                DecreaseRowsAbove(y + 1);
                y--;
                giveScore += FindObjectOfType<Ruleset>().rowValue;

                auRowExplode = Resources.Load<AudioClip>("Sounds/OldTazerFire");
                var au = new GameObject("auRowExplode");
                var audio = au.AddComponent<AudioSource>();
                audio.volume = .25f;
                audio.clip = auRowExplode;
                audio.Play();
            }
        }
        FindObjectOfType<Ruleset>().score += giveScore;
    }

    // End core.
    

    public static void SpawnExplosion(Transform pos)
    {
        Instantiate(pos.parent.GetComponent<Piece>().explosion, pos.position , pos.rotation);
    }
}
