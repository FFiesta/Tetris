﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour {
    
    // Time since last gravity tick
    float lastFall = 0;

    void Awake()
    {
        //al = GetComponent<AssetLoader>();
    }

    void Start()
    {
        // Default position not valid? Then it's game over
        if (!ignorePiece && !isValidGridPos())
        {
            Debug.Log("GAME OVER");
            FindObjectOfType<Ruleset>().EvaluateHighScore();
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update () {
        currentDownKeyTime += Time.deltaTime;
        if (!ignorePiece)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
            {
                // Modify position
                transform.position += new Vector3(-1, 0, 0);

                // See if valid
                if (isValidGridPos())
                    //Its valid. Update grid.
                    updateGrid();
                else
                    //It's not valid. Revert.
                    transform.position += new Vector3(1, 0, 0);
            }
            if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
            {
                // Modify position
                transform.position += new Vector3(1, 0, 0);

                // See if valid
                if (isValidGridPos())
                    //Its valid. Update grid.
                    updateGrid();
                else
                    //It's not valid. Revert.
                    transform.position += new Vector3(-1, 0, 0);
            }
            // Rotate
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            {
                transform.Rotate(0, 0, -90);

                //See if valid
                if (isValidGridPos())
                    //It's valid. Update grid.
                    updateGrid();
                else
                    // It's not valid. Revert.
                    transform.Rotate(0, 0, 90);
            }
            // Fall
            if ((Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)) && currentDownKeyTime > downDelay)
            {
                // Modify position
                transform.position += new Vector3(0, -1, 0);

                // See if valid
                if (isValidGridPos())
                {
                    // It's valid. Update grid.
                    updateGrid();
                }
                else
                {
                    // It's not valid. revert.
                    transform.position += new Vector3(0, -1, 0);

                    // Clear filled horizontal lines
                    Grid.DeleteFullRows();

                    // Spawn next Piece
                    FindObjectOfType<Spawner>().SpawnNext();

                    // Disable script
                    enabled = false;
                }
                currentDownKeyTime = 0;
                lastFall = Time.time;

            }

            // Move Downwards and Fall
            else if (Time.time - lastFall >= .8f)
            {
                // Modify position
                transform.position += new Vector3(0, -1, 0);

                // See if valid
                if (isValidGridPos())
                {
                    // It's valid. Update grid.
                    updateGrid();
                }
                else
                {
                    // It's not valid. revert.
                    transform.position += new Vector3(0, -1, 0);

                    // Clear filled horizontal lines
                    Grid.DeleteFullRows();

                    // Spawn next Group
                    FindObjectOfType<Spawner>().SpawnNext();

                    // Disable script
                    enabled = false;
                }

                lastFall = Time.time;
            }
        }

    }

    bool isValidGridPos()
    {
        foreach (Transform child in transform)
        {
            Vector3 v = Grid.RoundVec2(child.position);

            // Not inside Border?
            if (!Grid.InsideBorder(v))
                return false;

            // Block in grid cell (and not part of same group)?
            if (Grid.grid[(int)v.x, (int)v.y] != null && Grid.grid[(int)v.x, (int)v.y].parent != transform)
                return false;

        }
        return true;
    }

    void updateGrid()
    {
        // Remove old children from grid
        for (int y = 0; y < Grid.h; y++)
            for (int x = 0; x < Grid.w; x++)
                if (Grid.grid[x, y] != null)
                    if (Grid.grid[x, y].parent == transform)
                        Grid.grid[x, y] = null;

        // Add new children to grid
        foreach (Transform child in transform)
        {
            Vector3 v = Grid.RoundVec2(child.position);
            Grid.grid[(int)v.x, (int)v.y] = child;
        }
    }


    // End core.

    public ParticleSystem explosion;
    public bool ignorePiece;
    float downDelay = 0.04f;
    float currentDownKeyTime;

}
