﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ruleset : MonoBehaviour {

    public int rowValue;
    public int score;
    public Text scoreHUD;
    public Text hScoreHUD;
    public Text elapsedTimeHUD;
    public Text totalTimeHUD;
    public int hScore;
    public int totalTime;

    int elapsedTime;

    //bool safeQuit = false; 

    void Awake()
    {
        hScore = PlayerPrefs.GetInt("highScore");
        totalTime = PlayerPrefs.GetInt("totalTime");
    }

    void Update()
    {
        elapsedTime = (int)Time.time;

        elapsedTimeHUD.text = string.Format("Time\n {0:0}:{1:00}", elapsedTime / 60, elapsedTime % 60);

        //Debug.Log(Time.timeScale);
        //Debug.Log(safeQuit);
        scoreHUD.text = "Score\n" + score;
        hScoreHUD.text = "High score\n" + hScore;
        totalTimeHUD.text = string.Format("Total time\n{0:0}:{1:00}", totalTime / 60, totalTime % 60);
        /*if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!safeQuit)
            {
                Debug.Log("Jinkies!");
                Time.timeScale = 0;
                safeQuit = true;
            }
        }
        if(safeQuit && Input.GetKeyDown(KeyCode.Return))
        {
            Application.Quit();
        }
        else if (safeQuit && Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1;
            safeQuit = false;
        }*/

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

    }

    public void EvaluateHighScore()
    {
        if (hScore < score)
        {
            PlayerPrefs.SetInt("highScore", score);
            PlayerPrefs.SetInt("totalTime", elapsedTime);
        }
    }
}
