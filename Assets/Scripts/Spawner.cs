﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] pieces;
    public GameObject next;
    public GameObject previewGo;
    GameObject currentPreview;

	void Awake ()
    {
        int i = Random.Range(0, pieces.Length);

        Instantiate(pieces[i], transform.position, Quaternion.identity);

        PreviewNext();
        currentPreview.transform.parent = previewGo.transform;
    }
    

    public void SpawnNext()
    {
        Instantiate(next, transform.position, Quaternion.identity);
        PreviewNext();
    }

    public void PreviewNext()
    {
        int i = Random.Range(0, pieces.Length);
        next = pieces[i];
        if (currentPreview != null)
        {
            Destroy(currentPreview);
        }
        currentPreview = Instantiate(next, previewGo.transform.position, Quaternion.identity);
        var piecepview = currentPreview.GetComponent<Piece>();
        piecepview.ignorePiece = true;
    }
}
